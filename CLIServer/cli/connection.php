<?php
include __DIR__ . '/vendor/autoload.php';

class ConnectionBD
{
    protected $connStr;

    public function __construct()
    {
        $this->connStr = mysqli_connect('192.168.33.11', "adrian", "secret", "pirateYTDL");
    }
    public function connection_status()
    {
        
        // Check connection
        if (!$this->connStr) {
            die("Connection failed: " . mysqli_connect_error());
        }
        echo "Connected successfully";
        mysqli_close($this->connStr);
    }

    public function verifyDataFormat($url, $format)
    {
        $removed_first_one = substr($format, 1);
        $conn = $this->connStr;
        $query = "select * from videos where link = '$url' and state = 'Finalizado' and format = '$removed_first_one'";
        $result = $conn->query($query);
        if ($result->num_rows == 0) {
            return 0;
        } else {
            return 1;
           
        }
    }
    public function returnData($url, $format)
    {
        $removed_first_one = substr($format, 1);
        $conn = $this->connStr;
        $query = "select path_local from videos where link = '$url' and state = 'Finalizado' and format = '$removed_first_one'";
        $result = $conn->query($query);
        $rs = $result->fetch_assoc();
        return $rs;
    }

    public function update($id_video, $path_local)
    {
        $conn = $this->connStr;
        $query = "UPDATE videos SET state='Finalizado', path_local = '$path_local' WHERE id = '$id_video'";
        $conn->query($query);
    }

    public function updatetoinprocess($id_video)
    {
        $conn = $this->connStr;
        $query = "UPDATE videos SET state='En Proceso' WHERE id = '$id_video'";
        $conn->query($query);
    }

    public function updatetoerror($id_video)
    {
        $conn = $this->connStr;
        $query = "UPDATE videos SET state='Fallido' WHERE id = '$id_video'";
        $conn->query($query);
    }

}
//$con = new ConnectionBD;
//$con->updatetoinprocess(31);

?>
