<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/serviceCLI.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('192.168.33.11', 5672, 'adrian', 'secret');
$channel = $connection->channel();
$channel->queue_declare('hello', false, false, false, false);

echo " [*] Waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) {
    echo "[x] Processing the video \n";
    
    $obj = json_decode($msg->body);
    $serviceCLI = new ServiceCLI($obj);
};


$channel->basic_consume('hello', '', false, true, false, false, $callback);

while ($channel->is_consuming()) {
    $channel->wait();
}

$channel->close();
$connection->close();
?>
