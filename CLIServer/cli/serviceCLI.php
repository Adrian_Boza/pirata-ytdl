<?php
include __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/connection.php';

class ServiceCLI
{
    protected $user_id;
    protected $path;
    protected $url;
    protected $format;
    protected $raw_id_video;
    protected $path_local;
    protected $id_video;
    protected $errors;

    public function __construct($obj)
    {
        $this->errors = false;
        $this->user_id = $obj->{'user_id'};
        $this->id_video = $obj->{'video_id'};
        $this->url = $obj->{'link'};
        $this->path = '../../var/www/html/backupvideos/' . $this->user_id . '/';
        $this->format = '.' . $obj->{'format'};
        $this->path_local = "";
       // echo  $this->id_video;
        $this->initialize();

    }

    public function initialize()
    {
        $database = new ConnectionBD();
        $database->updatetoinprocess($this->id_video);
        $this->raw_id_video = $this->getIdVideoRaw($this->url);
        $this->path_local = "http://192.168.33.12/backupvideos/" . "$this->user_id/" . "$this->raw_id_video" . "$this->format";

        if ($database->verifyDataFormat($this->url, $this->format) == 0) {
            $this->download();
            if ($this->format != ".mp4") {
                    $this->convert();
                }

            if($this->checkoutError()){
                $database->updatetoerror($this->id_video);
                $this->errors = true;
           }
            
        } else if ($database->verifyDataFormat($this->url, $this->format) == 1) {
            $archivo = $database->returnData($this->url, $this->format);
            $route = $archivo["path_local"];
            
            $archivo = substr($route, 34);
            echo $archivo. "   .";
            $nameFile = substr($route, 35);
            echo "  2". $nameFile ."  ";
            $originalRoute = '../../var/www/html/backupvideos/' . $archivo;
            $toRoute = '../../var/www/html/backupvideos/' .  $this->user_id . "/" . $nameFile;
            $this->checkDir('../../var/www/html/backupvideos/' .  $this->user_id . "/");
            echo ($originalRoute ." ".$toRoute);
            link($originalRoute, $toRoute);
        }
        if(!$this->errors){
        $database->update($this->id_video, $this->path_local);
        } 
    }

    public function checkDir($makeDir)
    {
        if (file_exists($makeDir)) {
        } 
        else {
            mkdir($makeDir);

        }
    }

    public function download()
    {
        $this->checkDir('../../var/www/html/backupvideos/' . $this->user_id);
        $template = $this->path .$this->raw_id_video .'.mp4';
        $string = ('youtube-dl ' . escapeshellarg($this->url) . ' -f 18 -o ' .
            escapeshellarg($template));

        $descriptorspec = array(
            0 => array("pipe", "r"), // stdin
            1 => array("pipe", "w"), // stdout
            2 => array("pipe", "w"), // stderr
        );
        $process = proc_open($string, $descriptorspec, $pipes);
        $stdout = stream_get_contents($pipes[1]);
        fclose($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        $ret = proc_close($process);
        
        echo "[x] Download  completed \n";
    }
    public function getIdVideoRaw($url)
    {
        $url_parse = explode("=", $url);
        $name = $url_parse[1];
        return $name;
    }

    public function convert()
    {
        echo "[x] Starting  convert to " . $this->format . " \n";

        $file = "";

        foreach (glob($this->path . "*.*") as $filename) {
            $file = explode('/', $filename)[3];
            echo $file;
        }

        header("video/x-flv");
        passthru('ffmpeg -i ' . $this->path . $this->raw_id_video . ".mp4" . ' -preset veryfast ' . $this->path . $this->raw_id_video . $this->format);
        unlink($this->path . $this->raw_id_video . ".mp4");
        $this->path_local = "http://192.168.33.12/backupvideos/" . "$this->user_id/" . "$this->raw_id_video" . "$this->format";
        echo "[x]Finished \n";
    }


    public function checkout()
    {
        $file = "";

        foreach (glob($this->path . "*.*") as $filename) {
            $file = explode('/', $filename)[3];
            echo $file;
        }
    }

    public function checkoutError()
    {
        $this->path_local = "http://192.168.33.12/backupvideos/" . "$this->user_id/" . "$this->raw_id_video" . "$this->format";
        if(!file_exists($this->path_local)) {
            return false;
        }

        return true;
    }







}