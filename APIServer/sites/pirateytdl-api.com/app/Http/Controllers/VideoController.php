<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        
        $videos = auth()->user()->videos;
        return response()->json($videos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        return view('videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
      
        $request->validate([
            'link' => 'required',
            'format' => 'required',
        ]);

        $video = Video::create([
            'user_id' => auth()->id(),
            'link' => $request->link,
            'format' => $request->format,
            'state' => 'En Espera',
            'path_local'=>''
        ]);

        $connection = new AMQPStreamConnection('localhost', 5672, 'adrian', 'secret');
        $channel = $connection->channel();

        $channel->queue_declare('hello', false, false, false, false);
        
        $data = array('video_id' => $video->id, 'user_id' => auth()->user()->id, 'link' => $request->link, 'format' => $request->format);
        $stringify =  json_encode($data);
        $msg = new AMQPMessage($stringify);

        $channel->basic_publish($msg, '', 'hello');

        echo " [x] Sent 'Hello World!'\n";

        $channel->close();
        $connection->close();
        
        return response()->json($video ,201);

    }

   
    public function show($id)
    {
        $video = Video::where('user_id',auth()->user()->id)->where('id',$id)->first();
        return response()->json($video ,200);
    }

     
    public function destroy($id)
    {
        $video = Video::find($id);
        Video::destroy($id);
        return response()->json($video ,200);
    }



}
