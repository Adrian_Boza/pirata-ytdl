<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'link', 'state', 'format','user_id','path'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
