<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', 'HomeController@index')->name('home');

Route::get('/register', 'RegisterController@register_view');
Route::post('/register', 'RegisterController@register')->name('register');

Route::get('/login', 'LoginController@login_view');
Route::post('/login', 'LoginController@login')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::get('videos', 'VideoController@index')->name('videos');
Route::get('videos/create', 'VideoController@create')->name('videos.create');
Route::post('videos', 'VideoController@store')->name('videos');
Route::delete('videos/{id}', 'VideoController@destroy')->name('videos.destroy');
Route::get('videos/{id}', 'VideoController@show')->name('videos.show');
