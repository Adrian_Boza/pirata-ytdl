@extends('layouts.app')
@section('content')
<div class="card" style="">
    <div class="card-body">
    
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Download</h2>
            </div>
        </div>
    
   
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Enlace:</strong>
                    {{ $video->link }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Formato:</strong>
                    {{ $video->format }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Estado:</strong>
                    {{ $video->state }}
                </div>
            </div>
            @if ($video->state === 'Finalizado')

            <video  id="video" src=" {{ $video->path_local }}" controls>
                Tu navegador no implementa el elemento <code>video</code>.
            </video>
            @endif

        </div>
        <div class="pull-right">
                    <a class="btn btn-primary"  id="show-back" href="{{ route('videos') }}"> Back</a>
        </div>
        <br><br><br>
    </div>
</div>
@endsection