@extends('layouts.app')
 
@section('content')
<div class="card" style="">
  <div class="card-body">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Gestor de descargas</h2>
                </div>
                
            </div>
        </div>
    
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered table-striped mb-0">
                    <tr>
                        <th>ID</th>
                        <th>Enlace</th>
                        <th>Estado</th>
                        <th>Formato</th>
                        
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($videos as $video)
                    <tr>
                        <td>{{ $video->id }}</td>
                        <td>{{ $video->link }}</td>
                        <td>{{ $video->state }}</td>
                        <td>{{ $video->format }}</td>
                        
                        <td>
                            <form action="{{ url('videos.destroy',['id'=>$video->id]) }}" method="POST">
                                <a class="btn btn-info" id="show-table" href="{{ route('videos.show',['id'=>$video->id]) }}">Show</a>   
                                @if ($video->state === 'Finalizado')
                                <a type="submit" class="btn btn-warning" href="{{ $video->path_local }}" download="Video">Download</a>   
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
        </div>
        <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('videos.create') }}"> Crear una nueva descarga</a>
        </div>
    </div>
</div>
      
@endsection