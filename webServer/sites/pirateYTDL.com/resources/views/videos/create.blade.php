@extends('layouts.app')

@php $formats = ['mov', 'mpeg', 'avi', 'wmv', 'flv', 'webm', 'mp4', 'm4v']; @endphp

@section('content')
<div class="card" style="">
    <div class="card-body">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Agregar nueva descarga</h2>
                    </div>
                    
                </div>
            </div>
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <form action="{{ route('videos') }}" method="POST">
                @csrf
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Enlace de descarga:</strong>
                            <input type="text" name="link" class="form-control" placeholder="Link">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Formato:</strong>
                            <select class="form-control" name="format" required>
                                <option value="">Formato</option>
                                @foreach ($formats as $format)
                                    <option  value='{{ $format }}' selected='selected'> {{ $format }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-primary" id="back-create" href="{{ route('videos') }}"> Back</a>
                            <button type="submit" id="submit-create" class="btn btn-primary">Submit</button>
                            
                    </div>
                    
                </div>
            
            </form>
    </div>
</div>
@endsection