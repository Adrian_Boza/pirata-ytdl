<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'id','link', 'state', 'format','user_id','path_local'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
