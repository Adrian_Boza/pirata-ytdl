<?php

namespace App\Http\Middleware;

use Closure;
use Http;

class NotAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        if(!$response->failed()) {
            if($response->throw()->json()) {
                return redirect('home'); 
            } 
        } 
        return $next($request);
    }
}
