<?php

namespace App\Http\Middleware;

use Closure;
use Http;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        if($response->failed()) {
            session(['logged' => false]);
            return redirect('login'); 
        } 
        if(!$response->throw()->json()) {
            session(['logged' => true]);
            return redirect('login'); 
        } 
        return $next($request);
    }
}
