<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('not.auth', ['except' => ['logout']]);
    }

    function login(Request $request) {
        $request->validate([
            'email' => 'required|string|email', 
            'password' => 'required|string|min:8'
        ]);
        $response = Http::asForm()->post(config('app.api_url') . 'login', [
            'email' => $request['email'], 
            'password' => $request['password']
        ]);
        if ($response->failed()) { //errors >= 400
            return redirect()->back()->withErrors([
                'error' => 'Error en los datos'
            ]);
        } 
        $response = $response->throw()->json();
        session(['token' => $response['access_token']]);
        session(['logged' => true]);
        return redirect('videos');
    }

    function login_view() {
        return view('auth.login');
    }

    function logout() {
        $response = Http::withToken(session('token'))->post(config('app.api_url') . 'logout');
        if(!$response->failed()){
            session()->forget('token');
        }
        session(['logged' => false]);
        return redirect('login');
    }
}
