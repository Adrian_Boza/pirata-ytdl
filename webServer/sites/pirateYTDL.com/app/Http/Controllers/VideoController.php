<?php

namespace App\Http\Controllers;

use Http;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $response = Http::withToken(session('token'))->get(config('app.api_url') . 'video');
        $user = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        
        if($response->failed() || $user->failed()) {
            return redirect('home'); 
        } 
        
        $user = new User($user->throw()->json());
        $videos = [];

        foreach ($response->throw()->json() as $video){
            array_push($videos, new Video($video));
        }
        
        return view('videos.index')
           ->with([
               "videos" => $videos,
                "user" => $user
           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        if($user->failed()) {
            return redirect('home'); 
        } 
        $user = new User($user->throw()->json());
        return view('videos.create')->with([
             "user" => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'link' => 'required',
            'format' => 'required',
        ]);

        $response = Http::withToken(session('token'))->asForm()->post(config('app.api_url') . 'video', [
            'link' => $request->get('link'),
            'format' => $request->get('format')
        ]);
        
        if($response->failed()){
            return redirect()->route('videos')
            ->with('error', 'Error crear el video.');
        }
        return redirect()->route('videos')
            ->with('success', 'La descarga ha sido puesta en cola.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $video = Http::withToken(session('token'))->get(config('app.api_url') . 'video/' . $id);
        $user = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        
        if($video->failed() || $user->failed()) {
            return redirect('home'); 
        } 
      
        $user = new User($user->throw()->json());
        $video = new Video($video->throw()->json());
        
        return view('videos.show')
           ->with([
               "video" => $video,
                "user" => $user
           ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }


}
