<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = Http::withToken(session('token'))->get(config('app.api_url') . 'user-profile');
        //dd(session('token'));
        if($user->failed()) {
            return redirect('login'); 
        } 
        $user = new User($user->throw()->json());
        session(['logged' => true]);
        return view('home')->with([
            'user' => $user
        ]);
        
    }
}
