<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Http;
use Auth;
Use App\User;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('not.auth');
    }

    function register(Request $request) {
        
        $request->validate([
            'name' => 'required|string|between:2,100', 
            'email' => 'required|string|email', 
            'password' => 'required|string|min:8|confirmed'
        ]);
        $response = Http::asForm()->post(config('app.api_url') . 'register', $request->all());
        if ($response->failed()) { //errors >= 400
            return redirect()->back()->withErrors([
                'error' => 'Error en los datos'
            ]);
        }
        

        
        $response = $response->throw()->json();
        session(['token' => $response['access_token']]);
        session(['logged' => true]);
        return redirect()->route('videos');
    }

    function register_view() {
        
        return view('auth.register');
    }
    //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjFcL2FwaVwvYXV0aFwvcmVnaXN0ZXIiLCJpYXQiOjE2MTQ5MDc0NzQsImV4cCI6MTYxNDkxMTA3NCwibmJmIjoxNjE0OTA3NDc0LCJqdGkiOiJzWkRaenlHZXZOZ251enFJIiwic3ViIjoxMiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.3-L4qc6jgbDXsQcR2ZKp53dyPtX4Z6vmIbJQA-w5J8U
}
